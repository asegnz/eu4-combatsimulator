package eu4.comsim.gui;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.ProgressBar;
import org.eclipse.wb.swt.SWTResourceManager;

import com.google.common.eventbus.Subscribe;

import eu4.comsim.core.BattleInitRequest;
import eu4.comsim.core.BattleStats;
import eu4.comsim.core.BattleUpdateEvent;
import eu4.comsim.core.Battleline;
import eu4.comsim.core.DieRollProvider;
import eu4.comsim.core.General;
import eu4.comsim.core.Util;
import eu4.comsim.core.datatypes.Phase;
import lombok.Getter;

public final class DieMoraleComposite {
	
	@Getter private DieRollProvider drp = DieRollProvider.random();
	
	private CLabel dice_label;
	private ProgressBar moraleBar;
	private Label lblMorale;
	private double maxMorale;
	private CLabel phaseModifierLabel;
	private CLabel terrainModifierLabel;
	private boolean isAttacker;
	
	private Menu menu_chooseDRP;
	
	public DieMoraleComposite(boolean isAttacker) {
		this.isAttacker = isAttacker;
		Util.EVENTBUS.register(this);
	}
	
	/**
	 * @wbp.factory
	 * @wbp.factory.parameter.source layoutData gd_composite
	 */
	public Composite createComposite(Composite parent, Object layoutData) {
		Composite composite = new Composite(parent, SWT.BORDER);
		
		composite.setLayoutData(layoutData);
		composite.setLayout(new GridLayout(3, true));
		
		dice_label = createLabel(composite, SWTResourceManager.getImage(this.getClass(), "/icons/general/dice-1.png"),
				"Base roll (0-9) for the current phase. Right-click in setup mode to change values generated. \n The random setting will reuse the same sequence until selected another time ... \n DON'T CHANGE DURING SIMULATION, MIGHT CRASH!",
				true);
		menu_chooseDRP = new Menu(dice_label);
		
		MenuItem random = new MenuItem(menu_chooseDRP, SWT.RADIO);
		random.setText("Random (0-9)");
		random.setSelection(true);
		random.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				drp = DieRollProvider.random();
				dice_label.setText("?"); // TODO Will be overwritten by BattleUpdateEvent
				Util.EVENTBUS.post(BattleInitRequest.INSTANCE);
			}
			
		});
		
		for (int i = 0; i < 10; i++) {
			final int roll = i;
			MenuItem item = new MenuItem(menu_chooseDRP, SWT.RADIO);
			item.setText("Fixed (" + i + ")");
			item.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					drp = DieRollProvider.constant(roll);
					dice_label.setText(Integer.toString(roll));
					Util.EVENTBUS.post(BattleInitRequest.INSTANCE);
				}
			});
		}
		dice_label.setMenu(menu_chooseDRP);
		phaseModifierLabel = createLabel(composite, Phase.FIRE.getImage(),
				"Modifier from general skill difference for current phase", true);
		
		terrainModifierLabel = createLabel(composite,
				SWTResourceManager.getImage(this.getClass(), "/icons/general/terrain-cost.png"),
				"Modifier from terrain and leader maneuver difference", isAttacker);
		
		moraleBar = new ProgressBar(composite, SWT.NONE);
		GridData gd_moraleBar = new GridData(SWT.CENTER, SWT.CENTER, false, false, 5, 1);
		gd_moraleBar.widthHint = 250;
		moraleBar.setLayoutData(gd_moraleBar);
		moraleBar.setToolTipText("Current morale");
		
		lblMorale = new Label(composite, SWT.NONE);
		lblMorale.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 4, 1));
		lblMorale.setText("Morale: 2.75/5.22");
		
		return composite;
	}
	
	@Subscribe
	public void update(BattleUpdateEvent o) {
		int day = o.getDay();
		BattleStats stats = o.getStats(isAttacker);
		Battleline battleline = stats.bl;
		General otherGeneral = o.getStats(!isAttacker).bl.army.getGeneral();
		
		terrainModifierLabel.setText("-" + Integer.toString(o.getBattle().getEnvironmentPenalty()));
		terrainModifierLabel.setVisible(o.getBattle().getEnvironmentPenalty() > 0 && isAttacker);
		double averageMorale = stats.averageMorale();
		if (day == 0)
			maxMorale = averageMorale; // Little bit hacky
		lblMorale.setText(Util.DF.format(averageMorale) + "/" + Util.DF.format(maxMorale));
		moraleBar.setSelection((int) Math.round(averageMorale / maxMorale * 100));
		int gen_diff = battleline.army.getGeneral().difference(otherGeneral, Phase.from(day));
		phaseModifierLabel.setImage(Phase.from(day).getImage());
		String gen_diffString = Integer.toString(gen_diff);
		phaseModifierLabel.setVisible(gen_diff != 0);
		phaseModifierLabel.setText("+" + gen_diffString);
		
		dice_label.setText(Integer.toString(battleline.army.getDieRollProvider().getRoll(day)));
		
	}
	
	private static CLabel createLabel(Composite parent, Image img, String tooltip, boolean visible) {
		CLabel label = new CLabel(parent, SWT.BORDER | SWT.CENTER);
		GridData layoutData = new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 1);
		layoutData.heightHint = 40;
		layoutData.widthHint = 60;
		label.setLayoutData(layoutData);
		label.setImage(new Image(Display.getDefault(), img.getImageData().scaledTo(35, 35)));
		label.setText("+22");
		label.setToolTipText(tooltip);
		label.setVisible(visible);
		return label;
	}
}
