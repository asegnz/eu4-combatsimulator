package eu4.comsim.gui;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.ShellAdapter;
import org.eclipse.swt.events.ShellEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Slider;
import org.eclipse.wb.swt.SWTResourceManager;

import com.google.common.eventbus.Subscribe;

import eu4.comsim.core.Army;
import eu4.comsim.core.Battle;
import eu4.comsim.core.BattleInitRequest;
import eu4.comsim.core.BattleUpdateEvent;
import eu4.comsim.core.Util;
import eu4.comsim.core.datatypes.Phase;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

/**
 * Main composite of the GUI
 */
@Slf4j
@SuppressWarnings("synthetic-access")
public class MainComposite extends Composite {
	
	public static final String MAINFONT = "Segoe UI";
	
	private Battle battle;
	
	BattleComposite battleComposite;
	ArmyGroup attackerGroup;
	ArmyGroup defenderGroup;
	TerrainGroup trngrpTerrain;
	
	private CLabel lblDay;
	private CLabel phaseLabel;
	private Slider daySlider;
	
	private ResultsComposite results_defender;
	private ResultsComposite results_attacker;
	
	public MainComposite(Composite parent, int style) {
		super(parent, style);
		setFont(SWTResourceManager.getFont(MAINFONT, 7, SWT.NORMAL));
		Util.EVENTBUS.register(this);
		GridLayout layout_main = new GridLayout(2, false);
		setLayout(layout_main);
		
		getShell().addShellListener(new ShellAdapter() {
			
			@Override
			public void shellClosed(ShellEvent e) {
				try (FileOutputStream fos = new FileOutputStream(Util.PERSIST_FILE);
						ObjectOutputStream oos = new ObjectOutputStream(fos)) {
					
					oos.writeObject(attackerGroup.buildArmy());
					oos.writeObject(defenderGroup.buildArmy());
					oos.writeObject(trngrpTerrain.selectedTerrain);
					oos.writeObject(trngrpTerrain.getPenalty());
					oos.close();
				} catch (IOException ex) {
					log.error("Error during serialization on exit", ex);
				}
			}
		});
		
		battle = Battle.getFromFile();
		attackerGroup = new ArmyGroup("Attacker", battle.armyA);
		Group a_group = attackerGroup.createGroup(this);
		
		results_attacker = new ResultsComposite(true);
		Composite a_comp = results_attacker.createResultsComposite(this, SWT.NONE);
		
		trngrpTerrain = new TerrainGroup(battle.terrain, battle.crossingPenalty);
		new Label(this, SWT.NONE);
		Group t_group = trngrpTerrain.createGroup(this);
		
		battleComposite = new BattleComposite();
		battleComposite.create(this);
		
		Button btnSimulate = new Button(this, SWT.NONE);
		btnSimulate.setToolTipText("Toggle between battle setup and step-by-step execution.");
		btnSimulate.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 2, 1));
		btnSimulate.setFont(SWTResourceManager.getFont(MainComposite.MAINFONT, 10, SWT.BOLD));
		btnSimulate.setImage(SWTResourceManager.getImage(MainComposite.class, "/icons/general/start-sim.png"));
		btnSimulate.setBackgroundImage(null);
		btnSimulate.setText("Simulate");
		
		Composite dayComp = createDayComposite();
		
		defenderGroup = new ArmyGroup("Defender", battle.armyB);
		Group d_group = defenderGroup.createGroup(this);
		
		results_defender = new ResultsComposite(false);
		Composite d_comp = results_defender.createResultsComposite(this, SWT.NONE);
		new Label(this, SWT.NONE);
		
		btnSimulate.addMouseListener(new MouseAdapter() {
			
			@Override
			public void mouseDown(MouseEvent e) {
				boolean simulateState = "Simulate".equalsIgnoreCase(btnSimulate.getText());
				battle = createBattle(BattleInitRequest.INSTANCE);
				if (simulateState)
					battle.run();
				daySlider.setMaximum(battle.getDuration() + 1);
				setEnabledRecursive(a_group, !a_group.isEnabled());
				setEnabledRecursive(d_group, !d_group.isEnabled());
				setEnabledRecursive(t_group, !t_group.isEnabled());
				dayComp.setVisible(!dayComp.isVisible());
				String imgString = simulateState ? "reset_3" : "start-sim";
				btnSimulate.setImage(
						SWTResourceManager.getImage(MainComposite.class, "/icons/general/" + imgString + ".png"));
				btnSimulate.setText(simulateState ? "Reset" : "Simulate");
				
				updateDay(0);
				
			}
		});
	}
	
	private Composite createDayComposite() {
		Composite composite_2 = new Composite(this, SWT.NONE);
		GridData gd_composite_2 = new GridData(SWT.CENTER, SWT.FILL, false, false, 3, 1);
		gd_composite_2.widthHint = 314;
		composite_2.setLayoutData(gd_composite_2);
		composite_2.setLayout(new GridLayout(2, true));
		
		lblDay = new CLabel(composite_2, SWT.BORDER | SWT.CENTER);
		GridData gd_lblDay = new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 1);
		gd_lblDay.widthHint = 120;
		lblDay.setImage(SWTResourceManager.getImage(this.getClass(), "/icons/general/time.png"));
		lblDay.setLayoutData(gd_lblDay);
		
		GridData gd_lblPhase = new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 1);
		gd_lblPhase.widthHint = 120;
		phaseLabel = new CLabel(composite_2, SWT.BORDER | SWT.CENTER);
		phaseLabel.setLayoutData(gd_lblPhase);
		phaseLabel.setImage(Phase.FIRE.getImage());
		
		daySlider = new Slider(composite_2, SWT.BORDER);
		daySlider.setThumb(1);
		log.debug("Battle day: {}, initializing slider from 0 to {}", battle.getDuration(), battle.getDuration());
		daySlider.setMaximum(battle.getDuration() + 1);
		daySlider.addSelectionListener(new SelectionAdapter() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				log.debug("Slider value changed to {}", daySlider.getSelection());
				updateDay(daySlider.getSelection());
			}
		});
		daySlider.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 2, 1));
		composite_2.setVisible(false);
		return composite_2;
	}
	
	/**
	 * Factory method, creating a new {@link MainComposite}. Assumes quietly that parent has a GridLayout, and sets it
	 * to fill the layout
	 * 
	 * @wbp.factory
	 */
	public static MainComposite create(Composite parent) {
		MainComposite mainComposite = new MainComposite(parent, SWT.NONE);
		mainComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		mainComposite.updateDay(0);
		return mainComposite;
	}
	
	@Subscribe
	public Battle createBattle(@SuppressWarnings("unused") BattleInitRequest bir) {
		if (attackerGroup == null || defenderGroup == null || trngrpTerrain == null || results_attacker == null
				|| results_defender == null)
			return null;
		Army armyA = attackerGroup.buildArmy();
		armyA.setDieRollProvider(results_attacker.getDma().getDrp());
		Army armyB = defenderGroup.buildArmy();
		armyB.setDieRollProvider(results_defender.getDma().getDrp());
		battle = new Battle(armyA, armyB, trngrpTerrain.getTerrain(), trngrpTerrain.getPenalty());
		Util.EVENTBUS.post(new BattleUpdateEvent(0, battle));
		return battle;
	}
	
	public void updateDay(int day) {
		lblDay.setText("Day: " + Integer.toString(day));
		phaseLabel.setText("Phase: " + Phase.from(day).name);
		phaseLabel.setImage(Phase.from(day).getImage());
		log.debug("Day {}:, Casualties for attacker: {}", day, battle.battleLineA.history.get(day).casualties());
		daySlider.setSelection(day);
		Util.EVENTBUS.post(new BattleUpdateEvent(day, battle));
	}
	
	public static void setEnabledRecursive(@NonNull final Composite composite, final boolean enabled) {
		Control[] children = composite.getChildren();
		
		for (int i = 0; i < children.length; i++) {
			if (children[i] instanceof Composite) {
				setEnabledRecursive((Composite) children[i], enabled);
			} else {
				children[i].setEnabled(enabled);
			}
		}
		
		composite.setEnabled(enabled);
	}
	
}
