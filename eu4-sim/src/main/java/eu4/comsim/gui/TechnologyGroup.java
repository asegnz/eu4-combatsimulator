package eu4.comsim.gui;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.wb.swt.SWTResourceManager;

import eu4.comsim.core.BattleInitRequest;
import eu4.comsim.core.Technology;
import eu4.comsim.core.Util;
import lombok.Value;

/**
 * Holding all information regarding a nation/army's {@link Technology}, and providing input controls for the user to
 * change various aspects of it.
 */
public class TechnologyGroup {
	
	@Value
	public class TechChangedEvent {
		
		public final TechnologyGroup source;
		public final Technology newTech;
	}
	
	private static final String NAME = "Technology";
	
	/** SWT element */
	private final Group swtGroup;
	
	Technology.Group selectedTechGroup;
	
	private Spinner spinner_level;
	
	private Spinner spinner_morale;
	private Spinner spinner_disc;
	
	private Spinner spinner_infCA;
	private Spinner spinner_cavCA;
	private Spinner spinner_artCA;
	
	private Label label_totalMorale;
	private Label label_totalTactics;
	private Composite composite;
	
	public TechnologyGroup(Composite parent, Technology tech) {
		swtGroup = new Group(parent, SWT.NONE);
		swtGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		selectedTechGroup = tech.getGroup();
		swtGroup.setLayout(new GridLayout(4, false));
		swtGroup.setText(NAME);
		
		Label label_level = new Label(swtGroup, SWT.CENTER);
		label_level.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, true, 1, 1));
		label_level.setToolTipText("Military technology level");
		label_level.setImage(SWTResourceManager.getImage(TechnologyGroup.class, "/icons/general/Military_tech.png"));
		
		spinner_level = new Spinner(swtGroup, SWT.BORDER);
		spinner_level.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 1));
		spinner_level.setToolTipText("Select military tech level (1-32)");
		spinner_level.setMaximum(32);
		spinner_level.setMinimum(1);
		spinner_level.setSelection(tech.getLevel().level);
		spinner_level.addModifyListener(e -> {
			Technology newTech = parseTechnology();
			label_totalMorale.setText(Util.DF.format(newTech.getMorale()));
			label_totalTactics.setText(Util.DF.format(newTech.getTactics()));
			Util.EVENTBUS.post(new TechChangedEvent(TechnologyGroup.this, parseTechnology()));
		});
		Button button_techgroup = new Button(swtGroup, SWT.NONE);
		button_techgroup.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true, 2, 1));
		button_techgroup.setToolTipText("Right-click to choose technology group");
		button_techgroup.setImage(SWTResourceManager.getImage(Technology.Group.class, selectedTechGroup.imagePath));
		
		Menu menu_chooseTG = new Menu(button_techgroup);
		for (Technology.Group tg : Technology.Group.values()) {
			MenuItem item = new MenuItem(menu_chooseTG, SWT.RADIO);
			item.setText(tg.name);
			item.setSelection(tg == selectedTechGroup);
			item.addSelectionListener(new SelectionAdapter() {
				
				@Override
				public void widgetSelected(SelectionEvent e) {
					selectedTechGroup = Technology.Group.fromString(item.getText());
					button_techgroup
							.setImage(SWTResourceManager.getImage(Technology.Group.class, selectedTechGroup.imagePath));
					Util.EVENTBUS.post(new TechChangedEvent(TechnologyGroup.this, parseTechnology()));
					Util.EVENTBUS.post(BattleInitRequest.INSTANCE);
				}
			});
		}
		button_techgroup.setMenu(menu_chooseTG);
		
		Label separator = new Label(swtGroup, SWT.SEPARATOR | SWT.HORIZONTAL);
		separator.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 4, 1));
		
		Label label_morale = new Label(swtGroup, SWT.NONE);
		label_morale.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, true, 1, 1));
		label_morale.setToolTipText("Morale");
		label_morale.setImage(SWTResourceManager.getImage(TechnologyGroup.class, "/icons/general/Morale.png"));
		
		composite = new Composite(swtGroup, SWT.BORDER);
		composite.setLayout(new GridLayout(1, false));
		composite.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1));
		
		spinner_morale = new Spinner(composite, SWT.BORDER);
		spinner_morale.setToolTipText("Select morale multiplier (100 = 100% of base)");
		spinner_morale.setMaximum(500);
		spinner_morale.setSelection((int) (tech.getMoraleModifier() * 100));
		spinner_morale.addModifyListener(e -> {
			label_totalMorale.setText(Util.DF.format(parseTechnology().getMorale()));
			Util.EVENTBUS.post(BattleInitRequest.INSTANCE);
		});
		label_totalMorale = new Label(composite, SWT.NONE);
		label_totalMorale.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1));
		label_totalMorale.setToolTipText("Total morale (Base value x modifier)");
		label_totalMorale.setText(Util.DF.format(tech.getMorale()));
		
		Label label_infCA = new Label(swtGroup, SWT.NONE);
		label_infCA.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, true, 1, 1));
		label_infCA.setToolTipText("Infantry combat ability");
		label_infCA.setImage(SWTResourceManager.getImage(TechnologyGroup.class, "/icons/general/Infantry_power.png"));
		
		spinner_infCA = new Spinner(swtGroup, SWT.BORDER);
		spinner_infCA.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, true, 1, 1));
		spinner_infCA.setMaximum(500);
		spinner_infCA.setSelection((int) (tech.getInfCombatAbility() * 100));
		spinner_infCA.setToolTipText("Select infantry combat ability modifer (100 = 100% of base)");
		
		Label label_discipline = new Label(swtGroup, SWT.NONE);
		label_discipline.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, true, 1, 1));
		label_discipline.setToolTipText("Discipline");
		label_discipline.setImage(SWTResourceManager.getImage(TechnologyGroup.class, "/icons/general/Discipline.png"));
		
		spinner_disc = new Spinner(swtGroup, SWT.BORDER);
		spinner_disc.setDigits(2);
		spinner_disc.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 1));
		spinner_disc.setToolTipText("Select discipline modifier (100 = 100% of base value)");
		spinner_disc.setMaximum(300);
		spinner_disc.setSelection((int) (tech.getDiscipline() * 100));
		spinner_disc.addModifyListener(e -> label_totalTactics.setText(Util.DF.format(parseTechnology().getTactics())));
		
		Label label_cavCA = new Label(swtGroup, SWT.NONE);
		label_cavCA.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, true, 1, 1));
		label_cavCA.setToolTipText("Cavalry combat ability");
		label_cavCA.setImage(SWTResourceManager.getImage(TechnologyGroup.class, "/icons/general/Cavalry_power.png"));
		
		spinner_cavCA = new Spinner(swtGroup, SWT.BORDER);
		spinner_cavCA.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, true, 1, 1));
		spinner_cavCA.setMaximum(500);
		spinner_cavCA.setSelection((int) (tech.getCavCombatAbility() * 100));
		spinner_cavCA.setToolTipText("Select cavalry combat ability modifer (100 = 100% of base)");
		
		Label label_tactics = new Label(swtGroup, SWT.NONE);
		label_tactics.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, true, 1, 1));
		label_tactics
				.setImage(SWTResourceManager.getImage(TechnologyGroup.class, "/icons/general/Military_tactics.png"));
		label_tactics.setToolTipText("Military Tactics");
		label_totalTactics = new Label(swtGroup, SWT.NONE);
		label_totalTactics.setToolTipText(
				"Military tactics (Automatically determined from discipline and military technology level)");
		label_totalTactics.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 1));
		label_totalTactics.setText(Util.DF.format(tech.getTactics()));
		
		Label label_artCA = new Label(swtGroup, SWT.NONE);
		label_artCA.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, true, 1, 1));
		label_artCA.setToolTipText("Artillery combat ability");
		label_artCA.setImage(SWTResourceManager.getImage(TechnologyGroup.class, "/icons/general/Artillery_power.png"));
		
		spinner_artCA = new Spinner(swtGroup, SWT.BORDER);
		spinner_artCA.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, true, 1, 1));
		spinner_artCA.setToolTipText("Select artillery combat ability modifer (100 = 100% of base)");
		spinner_artCA.setMaximum(500);
		spinner_artCA.setSelection((int) (tech.getArtCombatAbility() * 100));
		
	}
	
	/**
	 * @return A new {@link Technology} generated from the mil tech level ( {@link #selectedTechGroup}) and spinner
	 *         values
	 */
	public Technology parseTechnology() {
		return new Technology(spinner_level.getSelection(), selectedTechGroup, spinner_disc.getSelection() / 100.0,
				spinner_morale.getSelection() / 100.0, spinner_infCA.getSelection() / 100.0,
				spinner_cavCA.getSelection() / 100.0, spinner_artCA.getSelection() / 100.0);
	}
	
}
