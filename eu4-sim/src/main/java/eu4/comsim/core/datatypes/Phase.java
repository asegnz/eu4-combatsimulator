package eu4.comsim.core.datatypes;

import org.eclipse.swt.graphics.Image;
import org.eclipse.wb.swt.SWTResourceManager;

/**
 * Combat phases in EU4. <i>Morale</i> is modeled as an individual "phase" ( for purposes of applying morale damage,
 * amongst others)
 */
public enum Phase {
	
	FIRE("Fire", 0),
	SHOCK("Shock", 1),
	MORALE("Morale", 2);
	
	/** Name of the phase */
	public final String name;
	/** Unique integer identifier, corresponds to index in Phase.values() */
	public final int id;
	
	private Phase(String name, int id) {
		this.name = name;
		this.id = id;
	}
	
	public Image getImage() {
		if (this == Phase.MORALE)
			return null;
		return SWTResourceManager.getImage(Phase.class, "/icons/general/Land_leader_" + name + ".png");
	}
	
	/**
	 * days (0,1,2) -> FIRE<br>
	 * days (3,4,5) -> SHOCK<br>
	 * days (6,7,8) -> FIRE<br>
	 * ...
	 *
	 * @return Phase corresponding to day (First day is 1, not 0)
	 */
	public static Phase from(int day) {
		// phase is a six day cycle (1,2,3 Fire; 4,5,6 Shock; repeat).
		return ((day % 6) < 3) ? FIRE : SHOCK;
	}
}
