package eu4.comsim.core;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * A die roll provider.
 */
public class DieRollProvider implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private int handicap = 0;
	private List<Integer> rolls = new ArrayList<>();
	private transient Iterator<Integer> iterator;
	
	private final Random rng = new Random();
	
	/**
	 * @return A DieRollProvider that returns random rolls in the range (0-9)
	 */
	public static final DieRollProvider random() {
		return DieRollProvider.from(new Random().ints(0, 10).limit(100).boxed().collect(Collectors.toList()));
	}
	
	/**
	 * @return A DieRollProvider that returns random rolls in the range (0-9)
	 */
	public static final DieRollProvider constant(int roll) {
		return DieRollProvider.from(IntStream.generate(() -> roll).limit(100).boxed().collect(Collectors.toList()));
	}
	
	/**
	 * Create from a given list of integers. They all need to be in range (0-9).
	 */
	public static final DieRollProvider from(List<Integer> source) {
		assert (source.stream().mapToInt(Integer::intValue).allMatch(i -> i >= 0 && i <= 9));
		DieRollProvider drp = new DieRollProvider();
		drp.iterator = source.iterator();
		drp.rolls.addAll(source);
		return drp;
	}
	
	private DieRollProvider() {
		// Hide constructor
	}
	
	/**
	 * @return The average of all dice rolls made
	 */
	public double averageRoll() {
		return rolls.stream().mapToInt(Integer::intValue).average().orElse(0.0);
	}
	
	/**
	 * @return The rolls generated (and stored) so far
	 */
	public List<Integer> rolls() {
		return rolls;
	}
	
	/**
	 * @return Next roll (0-9)
	 */
	public int getRoll(int day) {
		int listIndex = day / 3;
		if (rolls.size() > listIndex)
			return rolls.get(listIndex);
		int roll = iterator.hasNext() ? iterator.next() : rng.nextInt(10);
		roll += handicap;
		rolls.add(roll);
		return roll;
	}
	
	/**
	 * Adds a handicap to the die rolls
	 *
	 * @param value The handicap value. Positive values are added to all rolls of this General (up to a maximum of 15),
	 *        while negative ones are subtracted(minimum of 0).
	 *
	 */
	public void addHandicap(int value) {
		this.handicap = value;
	}
}
